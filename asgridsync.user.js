// ==UserScript==
// @name         Agent Stats -> The Grid sync
// @version      0.1
// @description  Sync data from AS -> Grid. Just go to the https://the-grid.org/r/?page=updatescore, enter your Agent Stats API key from https://www.agent-stats.com/preferences.php and your data should automatically sync into the form. Check it, then click submit.
// @author       neonninja, https://gitlab.com/neonninja/asgridsync/graphs/master
// @match        https://the-grid.org/r/?page=updatescore*
// @grant        none
// @homepageURL  https://gitlab.com/neonninja/asgridsync
// @downloadURL  https://gitlab.com/neonninja/asgridsync/raw/master/asgridsync.user.js
// @updateURL    https://gitlab.com/neonninja/asgridsync/raw/master/asgridsync.user.js
// @supportURL   https://gitlab.com/neonninja/asgridsync/issues

// ==/UserScript==

(function() {
    'use strict';

    // Grid category names to agent stat category names
    var statmap = {
        "statcat_ap": "ap",
        "statcat_exp_portalvisit": "explorer",
        "statcat_see_portaldisc": "discoverer",
        "statcat_see_seer": "seer",
        "statcat_dis_xm": "collector",
        "statcat_dis_opr": "recon",
        "statcat_hea_distance": "trekker",
        "statcat_bui_resdeploy": "builder",
        "statcat_con_linkscreated": "connector",
        "statcat_min_fieldscreated": "mind-controller",
        "statcat_bui_mucap": "illuminator",
        "statcat_bui_longlink": "binder",
        "statcat_bui_lafield": "country-master",
        "statcat_bui_xmrecharged": "recharger",
        "statcat_bui_portalcap": "liberator",
        "statcat_bui_unportalcap": "pioneer",
        "statcat_bui_moddeploy": "engineer",
        "statcat_pur_resdestroyed": "purifier",
        "statcat_com_portalsneut": "neutralizer",
        "statcat_pur_linksdestroyed": "disruptor",
        "statcat_pur_fieldsdestroyed": "salvator",
        "statcat_gua_maxtime": "guardian",
        "statcat_def_linkmain": "smuggler",
        "statcat_def_linkxdays": "link-master",
        "statcat_def_fieldheld": "controller",
        "statcat_def_muxdays": "field-master",
        "statcat_mis_completed": "specops",
        "statcat_mis_attended": "recruiter",
        "statcat_hac_hacks": "hacker",
        "statcat_hac_glyph": "translator",
        "statcat_hac_days": "sojourner",
        "statcat_men_agentrec": "recruiter"
    };

    function ASdialog(msg) {
        $("body").prepend("<div id='asdialog' style='position: fixed;top: 50%;left: 50%;width: 500px;margin-left: -250px;height: 50px;margin-top: -25px;background: gray;z-index: 9999'><div>" + msg + "</div>Enter your Agent Stats API Key (can be found at <a href='https://www.agent-stats.com/preferences.php'>https://www.agent-stats.com/preferences.php</a>). <input id='askey' type='text' /></div>");
        $("#askey").change(function() {
            askey = $(this).val();
            localStorage['AS-Key'] = askey;
            $("#asdialog").remove();
            getAS();
        });
    }

    function getAS() {
        console.log(askey);
        $.ajax("https://cors-anywhere.herokuapp.com/https://api.agent-stats.com/progress", {
            headers: {
                "AS-Key": askey
            },
            success: function(data) {
                if (data == "access denied") {
                    localStorage['AS-Key'] = "";
                    ASdialog("Invalid Agent Stats API Key.");
                    return;
                }

                var medals = data.mymedals;
                console.log(medals);

                var gridAP = $("input[name=statcat_ap]").val();
                var asAP = medals.ap.progression.total;
                if (asAP > gridAP) {
                    for (var k in statmap) {
                        var v = statmap[k];
                        var asV = medals[v].progression.total;
                        $("input[name=" + k + "]").val(asV);
                        console.log("Set " + k + " to " + asV + " from AS");
                    }
                    alert("Set values from Agent Stats");
                }
            }
        });

    }

    var host = window.location.host;
    if (host == "the-grid.org") {
        var askey = localStorage['AS-Key'];
        if (!askey) {
            ASdialog("");
        } else {
            getAS();
        }
    }
})();